Fixes xfce4-panel bug which draws windows under panel on external monitors. This is a modified PKGBUILD file for Arch Linux.

PKGBUILD is a modified version of the official repo version which applies the patch file linked 
in the following bug report: 

https://bugzilla.xfce.org/show_bug.cgi?id=9693

## How to install

The following commands will build and install the patch as per the modified PKGBUILD file. the `makepkg` command will download the xfce4-panel repo from the link listed in the file.

```
git clone https://github.com/tinmice/xfce4-panel-fix.git
makepkg PKGBUILD
pacman -Rdd xfce4-panel
pacman -U xfce4-panel-4.12.1-1-x86_64.pkg.tar
xfce4-panel -r
```
or

## Modify the PKGBUILD yourself

PKGBUILD modifications are described below, not needed to for patch implementation as described above. Original PKGBUILD taken from https://www.archlinux.org/packages/extra/x86_64/xfce4-panel/

Create `panel.patch` in the PKGBUILD directory, taken from the patch listed in the bug report https://bugzilla.xfce.org/attachment.cgi?id=6560

```
diff --git a/panel/panel-window.c b/panel/panel-window.c
index c21aac3..d0c91f0 100644
--- a/panel/panel-window.c
+++ b/panel/panel-window.c
@@ -2104,18 +2104,18 @@ panel_window_screen_layout_changed (GdkScreen   *screen,
               || (window->struts_edge == STRUTS_EDGE_RIGHT
                   && b.x + b.width > a.x + a.width))
             {
-              dest_y = MAX (a.y, b.y);
-              dest_h = MIN (a.y + a.height, b.y + b.height) - dest_y;
-              if (dest_h > 0)
+              dest_x = MAX (a.x, b.x);
+              dest_w = MIN (a.x + a.width, b.x + b.width) - dest_x;
+              if (dest_w > 0)
                 window->struts_edge = STRUTS_EDGE_NONE;
             }
           else if ((window->struts_edge == STRUTS_EDGE_TOP && b.y < a.y)
                    || (window->struts_edge == STRUTS_EDGE_BOTTOM
                        && b.y + b.height > a.y + a.height))
             {
-              dest_x = MAX (a.x, b.x);
-              dest_w = MIN (a.x + a.width, b.x + b.width) - dest_x;
-              if (dest_w > 0)
+              dest_y = MAX (a.y, b.y);
+              dest_h = MIN (a.y + a.height, b.y + b.height) - dest_y;
+              if (dest_h > 0)
                 window->struts_edge = STRUTS_EDGE_NONE;
             }
         }
```

Modify the PKGBUILD file:

Add `"panel.patch"` to `source(...)`

`sha256sum panel.patch` and add to `sha256sums=(...)`

Add a `prepare(...)` section to apply the patch, as follows:

```
prepare() {
 cd $pkgname-$pkgver
 patch -Np1 -i "${srcdir}/panel.patch"
}
```
Final PKGBUILD should then look as the version in this repository.

After that follow the instructions from `makepkg PKGBUILD` onwards to build and install  your modified version of xfce4-panel. 



